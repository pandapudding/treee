#ifndef tree_h
#define tree_h

struct tree_node {
	int* val;
	struct tree_node* left;
	struct tree_node* right;
};

void t_insert (struct tree_node* curr, int* val);
void* t_search (struct tree_node* curr, int* val);
void t_print (struct tree_node* curr);

#endif