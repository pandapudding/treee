#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

int main () {
	int a = 25;
	int* q = &a;
	int* p;
	int array [] = {4,8,3,7,11,42,36,29};
	struct tree_node root = { // Dummy-Knoten
		.val = q,
		.left = NULL,
		.right = NULL,
	};
	for (int i = 0; i < 8; ++i) // alle Elemente des Arrays werden in den Baum eingefügt
	{
		p = &array[i];
		t_insert(&root,p);
	}
	t_print(&root);
}