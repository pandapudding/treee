#include <stdio.h>
#include <stdlib.h>
#include "tree.h"

void t_insert (struct tree_node* curr, int* val) {
	if (curr == NULL || val == NULL) return;
	else while ((curr->left != NULL) || (curr->right != NULL)) {
		if (*val == *(curr->val)) return;
		else if (*val < *(curr->val)) {
			if (curr->left != NULL) curr = curr->left;
			else break;
		}
		else {
			if (curr->right != NULL) curr = curr->right;
			else break;
		}
	}
	if (curr == NULL) return;
	else {
		struct tree_node* new = malloc (sizeof(struct tree_node));
		new->left = NULL;
		new->right = NULL;
		new->val = val;
		if (*val < *(curr->val)) curr->left = new;
		else curr->right = new;
	}
}

void* t_search (struct tree_node* curr, int* val) {
	if (curr == NULL) return NULL;
	while (curr != NULL) {
		if (*val == *(curr->val)) return curr;
		else if (*val < *(curr->val)) curr = curr->left;
		else curr = curr->right;
	}
	return NULL;
}

void t_print (struct tree_node* curr) { // Inorder-Traversierung
	if (curr == NULL) return;
	t_print(curr->left);
	printf("%d\n", *(curr->val));
	t_print(curr->right);
}